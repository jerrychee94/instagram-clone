<?php
use App\Mail\NewUserWelcomeMail;

Auth::routes(['verify' => true]);

Route::middleware(['Admin'=> 'App\Http\Middleware\AdminMiddleware'])->group(function(){

});

Route::middleware(['verified'])->group(function () {
    Route::post('follow/{user}', 'FollowsController@store');
    Route::get('/p/create', 'PostsController@create');
    Route::post('/p', 'PostsController@store');
    Route::get('/p/{post}', 'PostsController@show');
    Route::get('/profile/{user}', 'ProfilesController@index')->name('profile.show');
    Route::get('/profile/{user}/edit', 'ProfilesController@edit')->name('profile.edit');
    Route::patch('/profile/{user}', 'ProfilesController@update')->name('profile.update');
    Route::get('notification-detail', 'NotificationController@detail')->name('notification.detail');
    Route::get('notification-list', 'NotificationController@list')->name('notification.list');
});

Route::get('/', 'PostsController@index');
