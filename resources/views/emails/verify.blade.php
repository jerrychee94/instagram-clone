@component('mail::layout')

{{-- Header --}}
@slot('header')
@component('mail::includes.header', ['url' => config('app.url')])
Jerrystagram
@endcomponent
@endslot

Welcome to aboard!

Please click the button below to verify your email address.

@component('mail::button', ['url' => $url])
Verify Email
@endcomponent

If you did not create an account, no further action is required.

Regards,

Jerry Chee
{{--    --}}{{-- Subcopy --}}
{{--    @slot('subcopy')--}}
{{--        @component('mail::subcopy')--}}
{{--            <!-- subcopy here -->--}}
{{--        @endcomponent--}}
{{--    @endslot--}}


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            All right reserve jerrystagram.com
        @endcomponent
    @endslot
@endcomponent

