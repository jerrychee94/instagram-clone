@extends('layouts.app')

@section('content')
    <form method="GET" id="notification-clicked"  action="{{route('notification.detail')}}">
        @csrf
        <table class="table table-hover table-notification">
            <thead>
                <tr>
                    <th scope="col">Description</th>
                    <th scope="col">Last updated</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($notifications as $notification)
                    <tr class="table-notification-row @if (!$notification->read_at) bg-light @endif">
                        <td>{{\App\User::find($notification->data['followed_by'])->username}} has followed you</td>
                        <td>{{$notification->created_at->diffForHumans()}}</td>
                    </tr>
                    <input type="hidden" name="followingUser" id="followingUser" value="{{$notification->data['followed_by']}}"/>
                    <input type="hidden" name="notificationID" id="notificationID" value="{{$notification->id}}"/>
                @endforeach
            </tbody>
        </table>
    </form>

@endsection
