<div class="dropdown" style="float: right; padding: 13px;">
    <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <i class="fa fa-bell" style="font-size: 20px; float: left; color: black"></i>
    </a>
    @if(count(Auth::user()->unreadNotifications))<span class="badge badge-danger">{{count(Auth::user()->unreadNotifications)}}</span>@endif

    <div class="notification__list dropdown-menu dropdown-menu-right" style="box-shadow: 0 0 5px rgba(0,0,0,.0975); width:500px !important;margin-top:20px; min-height: 100px;">
        <h3 class="shadow-sm p-3 mb-1 bg-light rounded">Notifications</h3>
            @foreach (Auth::user()->notifications->sortBy('created_at')->take(5) as $notification)
            <div class="container">
                <form method="GET" action="{{route('notification.detail')}}">
                    @csrf
                        <button class="list__item dropdown-item  list__item p-3 @if (!$notification->read_at) bg-light @endif">
                            <div class="row">
                                <div class="col-2">
                                    <img src="{{ \App\User::find($notification->data['followed_by'])->profile->profileImage() }}" class="rounded-circle w-100">
                                </div>
                                <div class="col-10">
                                        <span>{{\App\User::find($notification->data['followed_by'])->username}} has followed you</span>
                                        <span>{{$notification->created_at->diffForHumans()}}</span>
                                        <input type="hidden" name="followingUser" value="{{$notification->data['followed_by']}}"/>
                                        <input type="hidden" name="notificationID" value="{{$notification->id}}"/>
                                </div>
                            </div>
                        </button>
                </form>
            </div>
            @endforeach
        <p class="shadow-sm p-2 mb-0 bg-light rounded text-sm-center">
            <a href="{{route('notification.list')}}" role="button" aria-pressed="true">See all</a>
        </p>

    </div>

</div>
