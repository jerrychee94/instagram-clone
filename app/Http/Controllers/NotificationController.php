<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    public function detail(Request $request)
    {
        $followingUserId = $request->followingUser;
        Log::info('this is :' . $followingUserId);
        $notificationId = $request->notificationID;
        $notification = auth()->user()->notifications()->find($notificationId);

        $notification->markAsRead();
        $notification->save();

        return redirect()->route('profile.show', ['user' => $followingUserId]);
    }

    public function list()
    {
        $notifications = Auth::user()->notifications->sortBy('created_at');
        return view('header.partials.notification-list', compact('notifications'));
    }
}
