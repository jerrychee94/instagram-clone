<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
//
//    public function index()
//    {
//        $users = auth()->user()->following()->pluck('profiles.user_id');
//        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(5);
//        return view('posts.index', compact('posts'));
//    }

    public function index()
    {
        $user = auth()->user();
        if($user) {
            return redirect('/profile/'. $user->id);
        }else{
            return false;
        }
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $data = request()->validate([
            'caption' => 'required',
            'image' => ['required', 'image'],
        ]);

        $imageObj = request()->image;
        $imageName = time().'.'.$imageObj->getClientOriginalExtension();
        $destinationPath = public_path('storage/posts');
        $img = Image::make($imageObj->path());

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 666, true);
        }

        $img->fit(1200, 1200)->save($destinationPath.'/'.$imageName);

        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imageName,
        ]);

        return redirect('/profile/' . auth()->user()->id);
    }

    public function show(\App\Post $post)
    {
        return view('posts.show', compact('post'));
    }
}
