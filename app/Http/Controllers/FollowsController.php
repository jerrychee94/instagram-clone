<?php

namespace App\Http\Controllers;

use App\Events\UserFollowed;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FollowsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(User $user)
    {
        if(auth()->user()->id == $user->id){
            return response()->json([
                'status' => 'error',
                'msg'    => 'User should not follow same profile',
            ], 401);
        }
        auth()->user()->following()->toggle($user->profile);

        if (auth()->user()->following->contains($user->profile->id)) {
            event(new UserFollowed($user));
        }
    }
}
